import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cronometro',
  templateUrl: './cronometro.component.html',
  styleUrls: ['./cronometro.component.css']
})
export class CronometroComponent implements OnInit {

  @Input() inicio: number;
  contador: number;
  @Output() alertaCronometro: EventEmitter<string>;

  constructor() {
    this.contador = 12;
    this.alertaCronometro = new EventEmitter();
   }

  ngOnInit(): void {
    this.contador = this.inicio ? this.inicio : 10;
  }

  onClick(){
    // let identificador = this.contador;
    let interval = setInterval(() => {
      this.contador--;
      if (this.contador < 0) {
        // this.alertaCronometro.emit("Finaliza el cronometro con el numero " + identificador);
        clearInterval(interval);
        this.contador = this.inicio;
        this.alertaCronometro.emit("Finaliza el cronometro con el numero " + this.inicio);

      }
    },1000);
  }
  

}
