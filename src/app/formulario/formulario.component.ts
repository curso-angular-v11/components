import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Tarea } from '../models/tarea.model';

@Component({
  selector: 'formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  tarea: any;
  nuevaTarea: Tarea;
  // @Output() tareaCreada: EventEmitter<any>;
  @Output() tareaCreada: EventEmitter<Tarea>;

  constructor() { 
    this.tarea = {};
    this.nuevaTarea =new Tarea();
    this.tareaCreada = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onClickEnviar(){
    this.tareaCreada.emit(this.tarea)
  }
  onClick(){
    this.tareaCreada.emit(this.nuevaTarea);
    this.nuevaTarea = new Tarea();
  }
}
