import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  @Input() texto: string;

  constructor() { 
    console.log('*** Estoy en el constructor');
    console.log('Lo uso para inicializar propiedades');
    console.log(`TEXTO: ${this.texto}`);
  }

  ngOnChanges(changes: SimpleChanges){
    console.log('*** Estoy en el ngOnChanges');
    console.log(`TEXTO: ${this.texto}`);
    console.log(`Valor anterior: ${changes['texto'].previousValue}. Valor actual: ${changes['texto'].currentValue}`);
  }

  ngOnInit(): void {
    console.log('*** Estoy en el ngOnInit');
    console.log('Lo uso para lanzar las acciones que arrancan el componente');
    console.log(`TEXTO: ${this.texto}`);

  }

  ngAfterViewInit(){
    console.log('*** Estoy en el ngAfterViewInit');
    console.log('Lo uso para lanzar acciones iniciales dentro de la plantilla');
  }
}
