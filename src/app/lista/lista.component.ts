import { Component, OnInit, Input } from '@angular/core';
import { Tarea } from '../models/tarea.model';

@Component({
  selector: 'lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  // @Input() tareas: any[]
  @Input() tareas: Tarea[];

  constructor() { 
  }

  ngOnInit(): void {
    // let lista = "";
    // this.tareas.forEach((tarea)=>{
    //   lista += "<p> -Título: " + tarea.titulo + "-Descripción: " + tarea.descripcion + "</p>"
    // })
    // let elementoLista = document.getElementById("listaTareas");
    // elementoLista.innerHTML = lista;
  }

  mostrarTareas(){
    let resultado = '<ul>';
    this.tareas.forEach(tarea => {
      resultado += `<li>${tarea.titulo} - ${tarea.descripcion}</li>`;
    })
    resultado += '</ul>';
    return resultado;
  }
}
