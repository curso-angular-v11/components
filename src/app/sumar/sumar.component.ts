import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sumar',
  templateUrl: './sumar.component.html',
  styleUrls: ['./sumar.component.css']
})
export class SumarComponent implements OnInit {

  @Input() a : string;
  @Input() b : string;
  resultado : number;

  constructor() { 
    // this.a = 0;
    // this.b = 0;
    this.resultado = 0;

  }

  ngOnInit(): void {
  }

  onClick(){
      this.resultado = parseInt(this.a) + parseInt(this.b);
  }

}
