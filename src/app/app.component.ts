import { Component } from '@angular/core';
import { Tarea } from './models/tarea.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  titulos: string[];
  elementos: number[];
  campoTexto: string;
  persona: any;
  arrTareas: Tarea[];
  mainTexto: string;
  

  constructor() {
    this.titulos = ['Jurassic Park', 'Stars Wars', 'Jumanji'];
    this.elementos =[10,20,30,40]
    this.campoTexto = 'Valor inicial'
    this.persona = {};
    this.arrTareas = [];
    this.mainTexto = 'Texto desde el padre';
  }

  ngOnInit(){
    let cont = 0;
    setInterval(() => {
      this.mainTexto = `Texto ${cont++}`
    },2000)
  }

  onAlertaPulsada($event){
    console.log($event);
  }
  onAlertaCronometro($event){
    console.log($event);
  }
  onClick(){
    this.campoTexto = 'Valor desde el botón';
  }
  onTareaCreada($event){
    this.arrTareas.push($event)
    console.log(this.arrTareas);
  }
}
